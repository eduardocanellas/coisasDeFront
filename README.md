## Pasta com os Trabalhos voltados a Front End

### Trabalho de JS:
O arquivo 'pergola.html' pode ser ignorado, uma vez que foi um arquivo de referência para o conteúdo ensinado pelo Pérgola. 'trabalho.html' é a Página em que o trabalho de JS foi executado; abrir esse arquivo no navegador deve ser suficiente.

### Trabalho de POO:
Acredito que essa tarefa tenha sido cumprida, pois diversos elementos referentes à Orientação a Objetos foram usados. Entretanto, creio que o escopo dessa tarefa talvez tenha ficado um pouco 'em aberto', mas talvez tenha sido a intenção do instrutor para praticarmos a abstração da Orientação a Objetos.

 Detalhe: como o meu computador apresentou alguns problemas no momento de fazer uso do Typescript, tive de fazer o código em Java para não perder muito tempo tentando consertar isso. Moverei esforços para chegar nas aulas de Ionic com o Typescript funcionando corretamente.