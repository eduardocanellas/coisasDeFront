public class Cliente extends Pessoa{

    /* Atributos de Classe */
        private static int qtdClientes = 0;

    /* Atributos de Instância */
        private Conta conta;

    /* Construtor */
        public Cliente(String nome, String cpf, String rg,
            String email, Conta conta){
            
            super(nome, cpf, rg, email);
            this.conta = conta;

            Cliente.qtdClientes += 1;
        }

    /* Métodos de Instância */
    /* Getters */
            public String getNome(){
                return this.nome;
            }
}