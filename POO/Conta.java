public class Conta {

    /* Atributos de Classe */
	    private static int qtdContas = 0;
    
    /* Atributos de Instância */
        private int numero;
        private String agencia;
        private float saldo;

    /* Main (Não está sendo utilizada) */
        public static void main(String[] args) {
            System.out.println("oi");
            
            /*
                Conta conta = new Conta();

                conta.deposito(1000);
                conta.saque(50);
                float valorSaldo = conta.getSaldo();

                System.out.println(valorSaldo);
            */

            Conta conta1 = new Conta(123, "456");
            Cliente thales = new Cliente("Thales Machado", "000000", "123456", "email@emailcom", conta1);
            System.out.println(Conta.getQtdContas());
        }

    /* Construtor */
        public Conta(int numero, String agencia){
            this.numero = numero;
            this.agencia = agencia;
            this.saldo = 0;
            Conta.qtdContas += 1;
        }

    /* Métodos de Instância */
        public float saque(float valor){
            if (this.saldo >= valor){
                this.saldo -= valor;
                return valor;
            } 
            else {
                return 0;
            }
        }

        public void deposito(float valor){
            if (valor>= 0){
                this.saldo += valor;
            }
        }

    /* Métodos de Classe */
        public static int getQtdContas(){
            return Conta.qtdContas;
        }
    /* Getters */
        public float getSaldo(){
            return this.saldo;
        }
    
    /* Setters */
        public void setAgencia(String Agencia){
            this.agencia = Agencia;
        }

        public void setNumero(int numeroConta){
            this.numero = numeroConta;
        }

}