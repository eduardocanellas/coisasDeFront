public abstract class Pessoa{

    /* Atributos de Instância */
        protected String nome;
        protected String cpf;
        protected String rg;
        protected String email;
        protected Conta conta;

    /* Construtor */
        public Pessoa(String nome, String cpf, String rg, String email){
            this.nome = nome;
            this.cpf = cpf;
            this.rg = rg;
            this.conta = conta;
        }

    /* Métodos de Instância */
        public void darBomDia(){
            System.out.println("Tenha um bom dia!");
        }
}