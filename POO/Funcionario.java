public class Funcionario extends Pessoa{
    
    /* Atributos de Classe */
        private static int qtdFunc = 0;
    
    /* Atributos de Instância */
        private float salario;
        private float cargaHoraria;


    /* Main */
        public static void main(String[] args) {
            Funcionario rodolfo = new Funcionario("Rodolfo", "12345", "67890",
                "rodolfo@email.com", 2, 500);
            Conta novaConta = rodolfo.criarConta(500, "01234-5");
            Cliente exigente = new Cliente("Agatha", "9876", "1234", "agatha@email.com", novaConta);

            rodolfo.darBomDia();
            rodolfo.cumprimentarCliente();
            rodolfo.cumprimentarCliente(exigente.getNome());
            System.out.println(exigente);
            System.out.println(rodolfo);
        }

    /* Construtor */
        public Funcionario(String nome, String cpf, String rg,
            String email, float salario, float cargaHoraria){
            
            super(nome, cpf, rg, email);
            this.salario = salario;
            this.cargaHoraria = cargaHoraria;

            Funcionario.qtdFunc += 1;
        }

    /* Métodos de Instância */
        public void cumprimentarCliente(){
            System.out.println("Olá! Em que posso ajudar?");
        }
        public void cumprimentarCliente(String nomeCliente){
            String mensagem = "Olá, "+nomeCliente+"! Em que posso ajudar?";
            System.out.println(mensagem);
        }

        public Conta criarConta(int numero, String agencia){
            Conta conta = new Conta(numero, agencia);
            return conta;
        }
}